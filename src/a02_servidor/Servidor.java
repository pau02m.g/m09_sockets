package a02_servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


import com.fasterxml.jackson.databind.ObjectMapper;

public class Servidor {

	public static void main(String[] args){

		Random r = new Random();

		int portNumber = 60009;

		ArrayList<String> preguntas = new ArrayList<String>();
		for (int i = 1; i < 5; i++) {
			preguntas.add("string amb la " + i);
		}

		try (ServerSocket serverSocket = new ServerSocket(portNumber); Socket clientSocket = serverSocket.accept();

		) {
			Manager m_manager = new Manager(clientSocket);
			System.out.println("Connection established at port " + clientSocket.getPort() + " and local port "
					+ clientSocket.getLocalPort());

			StandartWrapper sw = new StandartWrapper();

			boolean flag = true;

			// BIENVEIDO
			m_manager.SendAndRecive(Protocolo.Bienvenido, Protocolo.ACK);

			while (flag) {
				// PREPARADO
				m_manager.SendAndRecive(Protocolo.Preparado, Protocolo.ACK);

				// JSON
				int rand = r.nextInt(4) + 1;
				sw.setPregunta("Pregunta de " + rand + " respostes");
				ArrayList<String> preguntasJSON = new ArrayList<String>();
				for (int i = 0; i < rand; i++) {
					preguntasJSON.add(preguntas.get(i));
				}
				sw.setRespostes(preguntasJSON);

				ObjectMapper mapper = new ObjectMapper();
				String json = mapper.writeValueAsString(sw);

				m_manager.SendAndRecive(json, Protocolo.ACK);

				// RECIVO RESPUESTA (NUMERO)
				String res = m_manager.Recive();
				int numRes = -1;
				
				try
				{
					numRes = Integer.parseInt(res);
				}catch(NumberFormatException e)
				{
					throw new PauException("Esperaba un entero y he recibido " + res);
				}
				int rand2 = r.nextInt(rand) + 1;
				m_manager.Send(Protocolo.ACK);

				// DIGO SI LA RESPUESTA ESTA BIEN
				if (rand2 == numRes) {
					m_manager.SendAndRecive(Protocolo.Bien, Protocolo.ACK);
				} else {
					m_manager.SendAndRecive(Protocolo.Mal, Protocolo.ACK);
				}

				// PREGUNTO SI QUEIRE SEGUIR

				m_manager.Send(Protocolo.VolsSeguir);

				
				String plegar = m_manager.Recive();
				switch (plegar) {
					case Protocolo.Plegar:
						flag = false;
						m_manager.Send(Protocolo.ACK);
						clientSocket.close();
						break;
					case Protocolo.Seguir:
						break;
					default:
						flag = false;
						clientSocket.close();
				}

			} // FIN DEL BUCLE

		} catch (IOException e) {
			System.out.println(
					"Exception caught when trying to listen to port " + portNumber + " or listening for a connection");
			System.out.println(e.getMessage());
		} catch (PauException e1) {
			System.err.println(e1);
			//m_manager.close();
		}
		finally {
			//m_manager.close();
			//clientSocket.close();
		}

	}

}
