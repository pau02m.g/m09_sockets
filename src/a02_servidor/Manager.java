package a02_servidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Manager {

	PrintWriter out;
	BufferedReader in;
	
	

	
	public Manager(Socket echoSocket) throws IOException{
		out = new PrintWriter(echoSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
	}
	


	

	
	public void Send(String mensaje) {
		out.println(mensaje);
	}
	
	
	public String Recive() throws IOException {
		return in.readLine();
	}
	
	public void Recive(String message) throws IOException, PauException{
		String servidor = Recive();
		if(!servidor.equals(message)) {
			throw new PauException("necesitava un " + servidor + " y me das un " + " message");
		}
	}
	
	public void SendAndRecive(String mensaje, String response) throws IOException, PauException {
		Send(mensaje);
		Recive(response);
	}
	
	public String SendAndRecive(String mensaje) throws IOException, PauException {
		Send(mensaje);
		return Recive();
	}
	
	
	public void ReciveAndSend(String servidor, String cliente) throws IOException, PauException {
		Recive(servidor);
		Send(cliente);
	}
	
	
	public void close() throws IOException {
		in.close();
		out.close();
	}
	
	
}
