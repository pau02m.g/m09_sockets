package a02_servidor;

import java.io.*;
import java.net.*;
import java.util.HashMap;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Cliente {

	public static void main(String[] args) throws PauException {

		//String hostName = "10.1.82.10";
		String hostName = "127.0.0.1";
		int puerto = 60009;
		
		

		try (	Socket echoSocket = new Socket(hostName, puerto);
				BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) 
		{
			
			Manager m_manager = new Manager(echoSocket);
			
			
				String userInput;
//				while ((userInput = stdIn.readLine()) != null) {
//					System.out.println("Received: " + in.readLine());
//					out.println(userInput);
//					System.out.println("Attempting to send: " + userInput);
//				}
			
				
				
				// BENVINGUT
				m_manager.ReciveAndSend(Protocolo.Bienvenido, Protocolo.ACK);
				
				boolean flag = true;
				
				while(flag) {
					//PREPARAT
					m_manager.ReciveAndSend(Protocolo.Preparado, Protocolo.ACK);
					
					
					//JSON
					String json = m_manager.Recive();
					MostrarJson(json);
					
					m_manager.Send(Protocolo.ACK);
					m_manager.Send(stdIn.readLine());
					m_manager.Recive(Protocolo.ACK);
					
					String respuesta = m_manager.Recive();

					System.out.println(" ____________________________");
					System.out.println("|                            |");
					System.out.println("|      La respuesta es:      |"); 
					System.out.println("|____________________________|");
					switch(respuesta) {
						case Protocolo.Bien:
							System.out.println(" ____________________");
							System.out.println("|                    |");
							System.out.println("|      CORRECTA      |"); // mal
							System.out.println("|____________________|");
							break;
						case Protocolo.Mal:
							System.out.println(" ____________________");
							System.out.println("|                    |");
							System.out.println("|       ERRONEA      |"); // mal
							System.out.println("|____________________|");
							break;
						default:
							throw new PauException("ESPERABA un clown o un bien :/");
					}
					m_manager.Send(Protocolo.ACK);
				
					m_manager.Recive(Protocolo.VolsSeguir);
					
					System.out.println("");
					System.out.println(" _______________________");
					System.out.println("|                       |");
					System.out.println("|QUIERES SEGUIR JUGANDO |");
					System.out.println("|_______________________|");
					System.out.println("|                       |");
					System.out.println("|   a) SI     b) NO     |");
					System.out.println("|_______________________|");
					
					String decision = stdIn.readLine(); 
					if(decision.toLowerCase().equals("si") || decision.toLowerCase().equals("a")) 
						m_manager.Send(Protocolo.Seguir);
					
					else if(decision.toLowerCase().equals("no") || decision.toLowerCase().equals("b")) {
						m_manager.Send(Protocolo.Plegar);
						flag = false;
					}
						
					
					else {
						System.out.println(" _______________________");
						System.out.println("|                       |");
						System.out.println("|      ˇIMPORTANTE!     |");
						System.out.println("|     Eres tontisimo    |");
						System.out.println("|_______________________|");
						m_manager.Send(Protocolo.Plegar);
						flag = false;
					}
					

					
					
					
				}

				
			
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host " + hostName);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to " + hostName);
			System.exit(1);
		}

	}

	private static void MostrarJson(String json) {
		
		ObjectMapper mapper = new ObjectMapper();				
		StandartWrapper Wrapper = null;
		

		try {
			Wrapper = mapper.readValue(json, StandartWrapper.class);
		} 
		catch (JsonParseException e) {
			e.printStackTrace();
		} 
		catch (JsonMappingException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		System.out.println(Wrapper.getPregunta());
		System.out.println();
		int cont = 1;
		for (String string : Wrapper.getRespostes()) {
			System.out.println(cont + " - " + string);
			cont++;
		}
	}

}
