package a01;

import java.util.ArrayList;

public class StandartWrapper {

	String pregunta;
	ArrayList<String> respostes;

	public StandartWrapper() {
		super();
		respostes = new ArrayList<String>();
	}

	public StandartWrapper(String pregunta, ArrayList<String> respostes) {
		super();
		this.pregunta = pregunta;
		this.respostes = respostes;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public ArrayList<String> getRespostes() {
		return respostes;
	}

	public void setRespostes(ArrayList<String> respostes) {
		this.respostes = respostes;
	}

	@Override
	public String toString() {
		return "StandartWrapper [pregunta=" + pregunta + ", respostes=" + respostes + "]";
	}
	
	
	
}
